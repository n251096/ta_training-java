package com.epam.training.student_nazarii_riabcheniuk.util;

public class StringUtils {

    public static boolean containsSubstring(String a, String b) {
        return ( a.toLowerCase().contains(b.toLowerCase()) ||
                b.toLowerCase().contains(a.toLowerCase()));
    }
}
