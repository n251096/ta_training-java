package com.epam.training.student_nazarii_riabcheniuk.config;

public interface constants {
    String PASTEBIN_BASE_URL = "https://pastebin.com/";
    String GOOGLE_CLOUD_BASE_URL = "https://cloud.google.com/";
}
