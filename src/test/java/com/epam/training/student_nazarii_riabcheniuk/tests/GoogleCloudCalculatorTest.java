package com.epam.training.student_nazarii_riabcheniuk.tests;

import com.epam.training.student_nazarii_riabcheniuk.models.CloudEngine;
import com.epam.training.student_nazarii_riabcheniuk.pages.GoogleCloudHomePage;
import com.epam.training.student_nazarii_riabcheniuk.util.CloudEngineBuilder;
import org.junit.Assert;
import org.testng.annotations.Test;

public class GoogleCloudCalculatorTest extends CommonConditions {

    @Test
    public void ensureCostEstimateSummaryDetailsMatchInput() throws InterruptedException {
        CloudEngine testSpecs = CloudEngineBuilder.fromProperties();

        CloudEngine estimatedSummary = new GoogleCloudHomePage(driver)
                .openPage()
                .searchForPlatformCalculator()
                .goToPricingCalculator()
                .submitParameters(testSpecs)
                .goToEstimateSummaryPage()
                .getEstimatedSummary();

        Assert.assertEquals(testSpecs, estimatedSummary);
    }
}
