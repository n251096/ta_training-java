package com.epam.training.student_nazarii_riabcheniuk.tests;

import com.epam.training.student_nazarii_riabcheniuk.driver.DriverSingleton;
import com.epam.training.student_nazarii_riabcheniuk.util.TestListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;


@Listeners({TestListener.class})
public class CommonConditions {

    protected WebDriver driver;

    @BeforeMethod()
    public void setUp()
    {
        driver = DriverSingleton.getDriver();
    }

    @AfterMethod(alwaysRun = true)
    public void stopBrowser() throws InterruptedException {
        Thread.sleep(10_000);
        DriverSingleton.closeDriver();
    }
}
