package com.epam.training.student_nazarii_riabcheniuk.tests;

import com.epam.training.student_nazarii_riabcheniuk.models.PastebinRecord;
import com.epam.training.student_nazarii_riabcheniuk.pages.PasteCreatePage;
import com.epam.training.student_nazarii_riabcheniuk.util.PasteBuilder;
import org.junit.Assert;
import org.testng.annotations.Test;

public class PastebinTest extends CommonConditions{
    @Test
    public void createPasteSimple() {
        PastebinRecord inputPaste = PasteBuilder.fromProperties();

        PastebinRecord createdPaste = new PasteCreatePage(driver)
                .openPage()
                .submitPaste(inputPaste)
                .getPasteDetails()
                .getCreatedPaste();

        Assert.assertEquals(inputPaste, createdPaste);
    }

    @Test
    public void createPasteWithBashScript() {
        PastebinRecord inputPaste = PasteBuilder.fromPropertiesWithBashScript();

        PastebinRecord createdPaste = new PasteCreatePage(driver)
                .openPage()
                .submitPaste(inputPaste)
                .getPasteDetails()
                .getCreatedPaste();

        Assert.assertEquals(inputPaste, createdPaste);
    }
}
