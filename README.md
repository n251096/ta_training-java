# ta_training-java

***

#### Smoke test
`mvn -Dbrowser=chrome -Denvironment=qa -Dsurefire.suiteXmlFiles=src\test\resources\testng-smoke.xml clean test`

#### All tests
`mvn -Dbrowser=chrome -Denvironment=qa -Dsurefire.suiteXmlFiles=src\test\resources\testng-all.xml clean test`

***

#### To run on a dev environment with failing testdata
`mvn -Dbrowser=chrome -Denvironment=dev -Dsurefire.suiteXmlFiles=src\test\resources\testng-smoke.xml clean test` 

`mvn -Dbrowser=chrome -Denvironment=dev -Dsurefire.suiteXmlFiles=src\test\resources\testng-all.xml clean test`

Screenshots of the failure moments are saved to target\screenshots
